// @ts-nocheck
export const state = () => {
  return {
    officers: [],
  }
}

export const mutations = {
  fetchOfficers(state, data) {
    state.officers = data
  },
}

export const actions = {
  // GET ALL
  async fetchOfficers(context) {
    const res = await this.$axios('http://localhost:9000/officers')
    context.commit('fetchOfficers', res.data)
  },

  // CREATE
  async addEmployees(context, data) {
    await this.$axios.post('http://localhost:9000/officer', {
      idCard: data.idCard,
      firstName: data.firstName,
      lastName: data.lastName,
      position: data.position,
      department: data.department,
    })
    const res = await this.$axios('http://localhost:9000/officers')
    context.commit('fetchOfficers', res.data)
  },

  // DELETE
  async deleteEmployees(context, id) {
    await this.$axios.delete(`http://localhost:9000/officer?id=${id}`)
    const res = await this.$axios('http://localhost:9000/officers')
    context.commit('fetchOfficers', res.data)
  },

  // PUT
  async putEmployees(context, data) {
    await this.$axios.put(`http://localhost:9000/officer?id=${data._id}`, {
      _id: data._id,
      idCard: data.idCard,
      firstName: data.firstName,
      lastName: data.lastName,
      position: data.position,
      department: data.department,
    })
    const res = await this.$axios('http://localhost:9000/officers')
    context.commit('fetchOfficers', res.data)
  },

  // SEARCH
  async findEmployeeByIdCard(context, data) {
    await this.$axios(
      `http://localhost:9000/officer-search?=${{
        idCard: data.idCard,
        firstName: data.firstName,
        lastName: data.lastName,
        position: data.position,
        department: data.department,
      }}`
    )
    const res = await this.$axios('http://localhost:9000/officers')
    context.commit('fetchOfficers', res.data)
  },
}

export const getters = {
  getData: (state: any) => state.officers,
}
