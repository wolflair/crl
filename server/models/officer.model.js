const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')
const Schema = mongoose.Schema

const officerSchema = new Schema(
  {
    idCard: String,
    firstName: String,
    lastName: String,
    department: String,
    position: String,
  },
  {
    timestamps: true,
    versionKey: false,
  },
)

// officerSchema.method('toJSON', function () {
//   const { _v, _id, ...object } = this.toObject()
//   object.id = _id
//   return object
// })

officerSchema.plugin(mongoosePaginate)

const OfficerModel = mongoose.model('Officer', officerSchema)

module.exports = OfficerModel
