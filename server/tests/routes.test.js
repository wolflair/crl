const app = require("../server");
const dbConfig = require("../config/db.config");
const mongoose = require("mongoose");
const request = require("supertest");

beforeEach((done) => {
  mongoose.connect(
    dbConfig.localURL,
    { useNewUrlParser: true, useUnifiedTopology: true },
    () => done()
  );
});

describe("List Officers", () => {
  it("returns a list of officers", async () => {
    const response = await request(app).get("/officers");
    expect(response.statusCode).toBe(200);
  });
});

describe("Create Officer", () => {
  it("create new officer", async () => {
    const response = await request(app).post("/officer").send({
      idCard: "01234567891011",
      firstName: "Pawaret",
      lastName: "Muengkaew",
      position: "Developer",
    });
    expect(response.statusCode).toBe(201);
  });
});

describe("Edit Officer", () => {
  it("edit officer", async () => {
    const response = await request(app).put("/officer").send({
      idCard: "01234567891011",
      firstName: "Apiwit",
      lastName: "Thammachai",
      position: "Maid",
    });
    expect(response.statusCode).toBe(200);
  });
});

describe("Delete Officer", () => {
  it("delete officer", async () => {
    const response = await request(app).delete("/officer");
    expect(response.statusCode).toBe(200);
  });
});
