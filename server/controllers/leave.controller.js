const LeaveModel = require('../models/leave.model')

exports.add = (req, res) => {
  const payload = req.body
  const leave = new LeaveModel(payload)
  leave
    .save()
    .then(res.status(201).end())
    .catch((e) => {
      res.status(500).send({ message: e.message })
    })
}

exports.findAll = (req, res) => {
  LeaveModel.find()
    .then((leave) => res.json(leave))
    .catch((e) => {
      res.status(500).send({ message: e.message })
    })
}
