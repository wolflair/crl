const express = require("express");
const router = express.Router();
const officers = require("../controllers/officer.controller");

router.get('/officers', officers.findAll);

router.get('/officers-pagination', officers.findAllPagination);

router.get('/officer-search', officers.findOne);

router.get('/search', officers.search);

router.post('/officer', officers.add);

router.put('/officer', officers.edit);

router.delete('/officer', officers.delete);

module.exports = router 
